const { ApolloServer } = require('apollo-server')
const { PubSub } = require('apollo-server')
const { PrismaClient } = require("@prisma/client")
const Query = require("./resolvers/Query")
const Mutation = require("./resolvers/Mutation")
const Subscription = require("./resolvers/Subscription")
const Link = require("./resolvers/Link")
const User = require("./resolvers/User")
const Vote = require("./resolvers/Vote")
const { getUserId } = require("./utils")

const fs = require('fs');
const path = require('path');
const prisma = new PrismaClient();
const pubsub = new PubSub();

// Actual implementation
// 1 for each type, DONT FORGET THE comma , for each root type
// data source
//  Apollo Server is capable of detecting, and automatically resolving any Promise object that is returned from resolver functions.
const resolvers = {
    Query,
    Mutation,
    Subscription,
    Link,
    User,
    Vote
}




// typeDefs can be provided either directly as a string or by referencing a file that contains your schema definition 
// FS and PATH - to connect the schema file schema.graphql
// attach an instance of Prisma Client to the context when initializing the server and then access it from inside the resolvers via the context argument.
const server = new ApolloServer({
    typeDefs: fs.readFileSync(
        path.join(__dirname, 'schema.graphql'), 'utf8'
    ),
    resolvers,
    context: ({ req }) => {
        return {
            ...req,
            prisma,
            pubsub,
            userId:
                req && req.headers.authorization
                    ? getUserId(req)
                    : null
        };
    }
})

//the resulth of LISTEN will be passed to the THEN
// { url } is the OBJECT DESTRUCTURING passed from LISTEN
server.listen().then(({ url }) =>
    console.log(`Server is runnng on ${url}`)
)

// not using object destructuring
// server.listen().then((data) =>
//     console.log(data.url)
// )

// DATA
// {
//     address: '::',
//     family: 'IPv6',
//     port: 4000,
//     server: <ref *1> Server {
//       maxHeaderSize: undefined,
//       insecureHTTPParser: undefined,
//       _events: [Object: null prototype] {
//         request: [Array],
//         connection: [Array],
//         listening: [Function: bound emit],
//         error: [Function: bound emit],
//         upgrade: [Function: upgrade]
//       },
//       _eventsCount: 5,
//       _maxListeners: undefined,
//       _connections: 0,
//       _handle: TCP {
//         reading: false,
//         onconnection: [Function: onconnection],
//         [Symbol(owner_symbol)]: [Circular *1]
//       },
//       _usingWorkers: false,
//       _workers: [],
//       _unref: false,
//       allowHalfOpen: true,
//       pauseOnConnect: false,
//       httpAllowHalfOpen: false,
//       timeout: 0,
//       keepAliveTimeout: 5000,
//       maxHeadersCount: null,
//       headersTimeout: 60000,
//       requestTimeout: 0,
//       stop: [Function: stop],
//       _pendingSockets: Map(0) {},
//       _connectionKey: '6::::4000',
//       [Symbol(IncomingMessage)]: [Function: IncomingMessage],
//       [Symbol(ServerResponse)]: [Function: ServerResponse],
//       [Symbol(kCapture)]: false,
//       [Symbol(async_id_symbol)]: 6
//     },
//     subscriptionsPath: '/graphql',
//     url: 'http://localhost:4000/',
//     subscriptionsUrl: 'ws://localhost:4000/graphql'
//   }
