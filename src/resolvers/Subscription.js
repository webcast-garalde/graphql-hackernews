
// asyncIterator - used by the GraphQL server to push the event data to the client.
function newLinkSubscribe(parent, args, context, info) {
    return context.pubsub.asyncIterator("NEW_LINK")
}

const newLink = {
    subscribe: newLinkSubscribe,
    resolve: payload => {
        return payload
    }
}

function newVoteSubscription(parent, args, context, info) {
    return context.pubsub.asyncIterator("NEW_VOTE")
}

const newVote = {
    subscribe: newVoteSubscription,
    resolve: payload => {
        return payload
    }
}

module.exports = {
    newLink,
    newVote
}