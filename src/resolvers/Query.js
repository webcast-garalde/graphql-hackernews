
const info = () => `This is the API of a Hackernews Clone`

async function feed(parent, args, context) {
    const where = args.filter
        ? {
            OR: [
                { description: { contains: args.filter } },
                { url: { contains: args.filter } },
            ],
        }
        : {}

    // filter first then skip and take
    const links = await context.prisma.link.findMany({
        where,
        skip: args.skip,
        take: args.take,
        orderBy: args.orderBy,
    })

    const count = await context.prisma.link.count({
        where,
        // skip: args.skip,
        // take: args.take,
    })

    // return the count and links in an object that adheres to the shape of a Feed object from your GraphQL schema.
    return {
        count,
        links
    }
}

// const link = (parent, args) => {
//     let link = links.findIndex(lnk => lnk.id == args.id)
//     return links[link]
// }


module.exports = {
    info,
    feed,
    // link
}