
const { PrismaClient } = require("@prisma/client")
const prisma = new PrismaClient()

//  to send queries to the database. You will write all your queries inside this function.
async function main() {
    const newLink = await prisma.link.create({
        data: {
            description: "Full Stack Tutorial for GraphQL",
            url: 'www.howtographql.com'
        }
    })

    const allLinks = await prisma.link.findMany()
    console.log(allLinks)
}

// Close the database connections when the script terminates.
main().catch(e => {
    throw e
}).finally(async () => {
    await prisma.$disconnect()
})